The main aim is to allow the customers to buy the products by viewing their virtual 3D projection in every possible direction without physically touching them.
Virtual shopping  android app allows the user to see 3D view of any object in every possible dimension by projecting an object on specific target and on hand . This helps the user to visualize the item he want to buy as if it is real without the need of physical item. Say if you want to look how a watch looks on your wrist , point your camera towards your wrist and click a snap , which then projects watch on to your wrist and if you want to place sofa near bed , point your camera towards bed and click a snap.

Instructions
1) Install the VirtualShopping.apk on android device ( Recommended RAM > 1 GB )
2) Open the app and point your camera to Target image ( IEEESB image)
3) A 3d Object will be projected on to the target . Move/Rotate your phone in any direction to view the object in every possible direction.
4) You can bring your hand close to the object projected , so that the object jumps from image target to your hand , which facilitates easy viewing of object.
Note :  This is only basic version , but in full fledged version to be developed no need of using any predefined image targets like IEEESB  , say if you want to place sofa near bed , point your camera towards bed and click a snap , and if you want to look how a watch looks on your wrist , point your camera towards your wrist and click a snap , which then projects watch on to your wrist.

