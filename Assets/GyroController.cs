﻿using UnityEngine;
public class GyroController : MonoBehaviour 
{
	#region [Private fields]
	
	private bool gyroEnabled = true;
	
	private readonly Quaternion baseIdentity =  Quaternion.Euler(90, 0, 0);
	private readonly Quaternion landscapeRight =  Quaternion.Euler(0, 0, 90);
	private readonly Quaternion landscapeLeft =  Quaternion.Euler(0, 0, -90);
	private readonly Quaternion upsideDown =  Quaternion.Euler(0, 0, 180);
	
	private Quaternion cameraBase =  Quaternion.identity;
	private Quaternion calibration =  Quaternion.identity;
	private Quaternion baseOrientation =  Quaternion.Euler(90, 0, 0);
	private Quaternion baseOrientationRotationFix =  Quaternion.identity;
	
	
	
	private Quaternion referanceRotation = Quaternion.identity;
	private bool debug = true;


	
	#endregion
	
	#region [Unity events]
	
	protected void Start () 
	{
		AttachGyro();
	}
	
	protected void Update() 
	{
		if (!gyroEnabled)
						return;
		transform.rotation = Quaternion.Slerp(transform.rotation,
		                                      cameraBase * ( ConvertRotation(referanceRotation * Input.gyro.attitude) * GetRotFix()), 0.2f);
	}
	
	
	protected void OnGUI()
	{
		if (debug) {

			
			if (GUILayout.Button ("On/off gyro: " + Input.gyro.enabled, GUILayout.Height (100))) {
				Input.gyro.enabled = !Input.gyro.enabled;
			}
			
			if (GUILayout.Button ("On/off gyro controller: " + gyroEnabled, GUILayout.Height (100))) {
				if (gyroEnabled) {
					DetachGyro ();
				} else {
					AttachGyro ();
				}
			}
			
			if (GUILayout.Button ("Reset base orientation", GUILayout.Height (80))) {
				ResetBaseOrientation ();
			}
			
			if (GUILayout.Button ("Close", GUILayout.Height (80))) {
				debug = false;
			}
			
	
		} else
			debugg (); 
	}
	private void debugg(){
		if (GUILayout.Button ("Open", GUILayout.Height (80))) {
			debug = true;
		}
	}
	#endregion
	
	#region [Public methods]
	private void AttachGyro()
	{
		gyroEnabled = true;
		ResetBaseOrientation();
		UpdateCalibration(true);
		RecalculateReferenceRotation();
	}
	
	private void DetachGyro()
	{
		gyroEnabled = false;
	}


	#endregion
	
	#region [Private methods]
	
	private void UpdateCalibration(bool onlyHorizontal)
	{
		if (onlyHorizontal)
		{
			var fw = (Input.gyro.attitude) * (-Vector3.forward);
			fw.z = 0;
			if (fw == Vector3.zero)
			{
				calibration = Quaternion.identity;
			}
			else
			{
				calibration = (Quaternion.FromToRotation(baseOrientationRotationFix * Vector3.up, fw));
			}
		}
		else
		{
			calibration = Input.gyro.attitude;
		}
	}

	
	private static Quaternion ConvertRotation(Quaternion q)
	{
		
		return new Quaternion(q.x, q.y, -q.z, -q.w);	
	}
	
	private Quaternion GetRotFix()
	{


		return Quaternion.identity;

	}
	
	private void ResetBaseOrientation()
	{
		baseOrientationRotationFix = GetRotFix();
		baseOrientation = baseOrientationRotationFix * baseIdentity;
	}
	
	private void RecalculateReferenceRotation()
	{
		referanceRotation = Quaternion.Inverse(baseOrientation)*Quaternion.Inverse(calibration);
	}
	
	#endregion
}